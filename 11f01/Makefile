CPU = auto
#socat  pty,link=/dev/virtualcom0,raw  tcp:192.168.254.254:8080&
#PORT ?= /dev/virtualcom0
PORT ?= /dev/ttyUSB1
SYSCLK ?= 11059200

# Usually SDCC's small memory model is the best choice.  If
# you run out of internal RAM, you will need to declare
# variables as "xdata", or switch to large model eg.
# --iram-size 256
SDCCCFLAGS = --model-small

# These settings control where the compiler will place the code
# and variables in memory.  The executable code will begin at
# 4000.  Internal ram usage for variables will begin at 30 (which
# is just after the 8051's 4 register banks and bit variables).
# Variables in external RAM ("xdata" type) will begin at 1000.
#ASLINKFLAGS = --code-loc 0x2000 --data-loc 0x30 --xram-loc 0x6000

SRC = main.c $(shell ls *.c)

OBJ=$(patsubst %.c,%.rel, $(SRC))

all: clean main

%.rel: %.c
	@hash sdcc 2>/dev/null || sudo apt-get install sdcc
	sdcc $(SDCCCFLAGS) -c $<

# The $^ is a shorthand syntax for file in $OBJ
# (or whatever the list of dependant files happens to be). 
# Two commands are needed... one to run sdcc, and another
# to run the "packihx" utility that cleans up the intel hex output. 
main: $(OBJ)
	sdcc $(SDCCCFLAGS) $(ASLINKFLAGS) $^
	packihx $@.ihx > $@.hex

flash:
	@hash virtualenv 2>/dev/null || sudo apt-get install python-virtualenv
	@[ ! -d "py3env" ] && virtualenv -p $(shell which python3) py3env || true
	@[ ! -x "py3env/bin/stcgal" ] && ./py3env/bin/pip3 install git+https://github.com/grigorig/stcgal.git@master --upgrade || true
	./py3env/bin/stcgal -p $(PORT) -P $(CPU) -t $(SYSCLK) main.hex

clean:
	rm -f *.hex *.ihx *.lnk *.lst *.map *.rel *.rst *.sym *.mem *.lk *.asm
	clear
