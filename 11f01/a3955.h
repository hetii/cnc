#ifndef A3955_H_INCLUDED
#define A3955_H_INCLUDED

#define PHASEA P3_7
#define D2A P1_2
#define D1A P1_1
#define D0A P1_0

#define PHASEB P3_0
#define D2B P1_7
#define D1B P1_6
#define D0B P1_5

#define STEP P3_3
#define DIR P3_4
#define ENA P3_1

#define eighth_step_table_size 32
#define quarter_step_table_size 16
#define half_step_table_size 8
#define full_step_table_size 4

uint8_t eighth_step_table[eighth_step_table_size] = {
  0b11001100,
  0b10111101,
  0b10101110,
  0b10011111,
  0b00001111,
  0b00011111,
  0b00101110,
  0b00111101,
  0b01001100,
  0b01011011,
  0b01101010,
  0b01111001,
  0b01110000,
  0b01110001,
  0b01100010,
  0b01010011,
  0b01000100,
  0b00110101,
  0b00100110,
  0b00010111,
  0b00000111,
  0b10010111,
  0b10100110,
  0b10110101,
  0b11000100,
  0b11010011,
  0b11100010,
  0b11110001,
  0b11110000,
  0b11111001,
  0b11101010,
  0b11011011
};

uint8_t quarter_step_table[quarter_step_table_size] = {
  0b11001100,
  0b10101110,
  0b00001111,
  0b00101110,
  0b01001100,
  0b01101010,
  0b01110000,
  0b01100010,
  0b01000100,
  0b00100110,
  0b00000111,
  0b10100110,
  0b11000100,
  0b11100010,
  0b11110000,
  0b11101010
};

uint8_t half_step_table[half_step_table_size] = {
  0b11001100,
  0b00001111,
  0b01001100,
  0b01110000,
  0b01000100,
  0b00000111,
  0b11000100,
  0b11110000
};

uint8_t full_step_table[full_step_table_size] = {
  0b11001100,
  0b01001100,
  0b01000100,
  0b11000100
};

void set_step(uint8_t dir, uint8_t *step_table, int8_t step_table_size){
  static int8_t step=0;
  static uint8_t last_dir=1;
  int8_t s=0;

  if (dir == 1){
    if (last_dir != dir) {
       step = step+2;
       last_dir = dir;
    }
    if (step > step_table_size-1) step = 0;
    s = step++;
  } else {
    if (last_dir != dir) {
      step = step-2;
      last_dir = dir;
    }
    if (step < 0) step = step_table_size-1;
    s = step--;
  }
    PHASEA = ((step_table[s]>>7) & 0x01);
    D2A = ((step_table[s]>>6) & 0x01);
    D1A = ((step_table[s]>>5) & 0x01);
    D0A = ((step_table[s]>>4) & 0x01);
    PHASEB = ((step_table[s]>>3) & 0x01);
    D2B = ((step_table[s]>>2) & 0x01);
    D1B = ((step_table[s]>>1) & 0x01);
    D0B = ((step_table[s]>>0) & 0x01);
}

void set_off(void){
  PHASEA = D2A = D1A = D0A = PHASEB = D2B = D1B = D0B = 0;
}
/*
  /------------------------------------------------------------------------------------------------------------\
  | Full | Half | Quarter | Eighth |                BRIDGE A             |                BRIDGE B             |
  | step | step | step    | step   | PHASEA | D2A | D1A | D0A | I LOAD A | PHASEB | D2B | D1B | D0B | I LOAD B |
  |------------------------------------------------------------------------------------------------------------|
  |               OFF              | X      | L   | L   | L   | 0        | X      | L   | L   | L   | 0        |
  |------------------------------------------------------------------------------------------------------------|
  | 1    | 1    | 1       | 1      | H      | H   | L   | L   | 70.7     | H      | H   | L   | L   | 70.7     |
  |      |      |         | 2      | H      | L   | H   | H   | 55.5     | H      | H   | L   | H   | 83.1     |
  |      |      | 2       | 3      | H      | L   | H   | L   | 38.2     | H      | H   | H   | L   | 92.4     |
  |      |      |         | 4      | H      | L   | L   | H   | 19.5     | H      | H   | H   | H   | 100      |
  |      | 2    | 3       | 5      | X      | L   | L   | L   | 0        | H      | H   | H   | H   | 100      |
  |      |      |         | 6      | L      | L   | L   | H   | -19.5    | H      | H   | H   | H   | 100      |
  |      |      | 4       | 7      | L      | L   | H   | L   | -38.2    | H      | H   | H   | L   | 92.4     |
  |      |      |         | 8      | L      | L   | H   | H   | -55.5    | H      | H   | L   | H   | 83.1     |
  |------------------------------------------------------------------------------------------------------------- 
  | 2    | 3    | 5       | 9      | L      | H   | L   | L   | -70.7    | H      | H   | L   | L   | 70.7     |
  |      |      |         | 10     | L      | H   | L   | H   | -83.1    | H      | L   | H   | H   | 55.5     |
  |      |      | 6       | 11     | L      | H   | H   | L   | -92.4    | H      | L   | H   | L   | 38.2     |
  |      |      |         | 12     | L      | H   | H   | H   | -100     | H      | L   | L   | H   | 19.5     |
  |      | 4    | 7       | 13     | L      | H   | H   | H   | -100     | X      | L   | L   | L   | 0        |
  |      |      |         | 14     | L      | H   | H   | H   | -100     | L      | L   | L   | H   | -19.5    |
  |      |      | 8       | 15     | L      | H   | H   | L   | -92.4    | L      | L   | H   | L   | -38.2    |
  |      |      |         | 16     | L      | H   | L   | H   | -83.1    | L      | L   | H   | H   | -55.5    |
  |------------------------------------------------------------------------------------------------------------|
  | 3    | 5    | 9       | 17     | L      | H   | L   | L   | -70.7    | L      | H   | L   | L   | -70.7    |
  |      |      |         | 18     | L      | L   | H   | H   | -55.5    | L      | H   | L   | H   | -83.1    |
  |      |      | 10      | 19     | L      | L   | H   | L   | -38.2    | L      | H   | H   | L   | -92.4    |
  |      |      |         | 20     | L      | L   | L   | H   | -19.5    | L      | H   | H   | H   | -100     |
  |      | 6    | 11      | 21     | X      | L   | L   | L   | 0        | L      | H   | H   | H   | -100     |
  |      |      |         | 22     | H      | L   | L   | H   | 19.5     | L      | H   | H   | H   | -100     |
  |      |      | 12      | 23     | H      | L   | H   | L   | 38.2     | L      | H   | H   | L   | -92.4    |
  |      |      |         | 24     | H      | L   | H   | H   | 55.5     | L      | H   | L   | H   | -83.1    |
  |------------------------------------------------------------------------------------------------------------|
  | 4    | 7    | 13      | 25     | H      | H   | L   | L   | 70.7     | L      | H   | L   | L   | -70.7    |
  |      |      |         | 26     | H      | H   | L   | H   | 83.1     | L      | L   | H   | H   | -55.5    |
  |      |      | 14      | 27     | H      | H   | H   | L   | 92.4     | L      | L   | H   | L   | -38.2    |
  |      |      |         | 28     | H      | H   | H   | H   | 100      | L      | L   | L   | H   | -19.5    |
  |      | 8    | 15      | 29     | H      | H   | H   | H   | 100      | X      | L   | L   | L   | 0        |
  |      |      |         | 30     | H      | H   | H   | H   | 100      | H      | L   | L   | H   | 19.5     |
  |      |      | 16      | 31     | H      | H   | H   | L   | 92.4     | H      | L   | H   | L   | 38.2     |
  |      |      |         | 32     | H      | H   | L   | H   | 83.1     | H      | L   | H   | H   | 55.5     |
  \------------------------------------------------------------------------------------------------------------/
*/

#endif
