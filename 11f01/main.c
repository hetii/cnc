#include <mcs51/8051.h>
#include <stc12.h>
#include <stdio.h>
#include <ctype.h>
#include <stdint.h>
#include "a3955.h"

/* Table: Configuration of I/O port mode.
 * where x = 0 ~ 4 (port number), and y = 0 ~7 (port pin).
 *
 * PxM0.n | PxM1.n | Port Mode
 * 0      | 0      | Quasi-bidirectional (tranditional 8051 MCU I/O port mode)
 * 1      | 0      | Push-Pull output
 * 0      | 1      | Input Only (High-impedance)
 * 1      | 1      | Open-Drain Output
 */

void _delay_ms(uint8_t ms){	
    // i,j selected for fosc 11.0592MHz, using oscilloscope
    // the stc-isp tool gives inaccurate values (perhaps for C51 vs sdcc?)
    // max 255 ms
    uint8_t i, j, k,l;

    do {
    	i = 255;
    	j = 225;
      k = 255;
      l = 200;
    	do
    	{
    		while (--j);
        //while (--k);
        while (--l);
    	} while (--i);
    } while (--ms);
}

void main() {
  uint8_t last_step=0;

  P3M0 |= (1<<7)|(1<<0);  // high
  P3M1 &= ~(1<<7)|(1<<0); // low

  P1M0 |= (1<<0)|(1<<1)|(1<<2)|(1<<5)|(1<<6)|(1<<7);  // high
  P1M1 &= ~(1<<0)|(1<<1)|(1<<2)|(1<<5)|(1<<6)|(1<<7); // low

  P3M0 &= ~(1<<3)|(1<<4)|(1<<1);
  P3M1 |= (1<<3)|(1<<4)|(1<<1);

  set_off();

  for(;;) {
    if (ENA == 0){
      if (STEP == 0 && last_step != 1){
        last_step = 1;
        set_step(DIR, eighth_step_table, eighth_step_table_size);
        //set_step(DIR, full_step_table, full_step_table_size);
      }
      if (STEP == 1) last_step = 0;
    } else {
      set_off();
    }
  }

}
