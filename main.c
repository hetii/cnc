#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "io.h"
/*
 * Examples:
 *
 * #define LED_DIR BITP(DDRB, PB4); // LED_DIR is "variable" pointing to single bit (PB4) in DDRB
 * #define LED_STATE BITP(PORTB, PB4); // LED_STATE points to single bit (PB4) in PORTB
 * LED_DIR = OUT; // Set pin as output
 * LED_STATE = ON; // Switch LED on
 * 
 * BITS_SET(DDRB, PB3, PB5, PB7); // Set bits PB3, PB5 and PB7 in DDRB, leave other bits untouched.
 * BITS_SET_EX(DDRB, PB7); // Sets only PB7 in DDRB, other bits are zeroed (set exclusive).
 * BITS_CLEAR(PORTB, PB4); // Clear bit PB4 in PORTB, leave other bits untouched.
 */

#define PHASEA PC5
#define D0A PC4
#define D1A PC3
#define D2A PC2

#define PHASEB PC1
#define D0B PC0
#define D1B PB2
#define D2B PB1

#define LED BITP(PORTB, PB0);


void move(int8_t p){
  switch(p){
    case 0:
      BITS_SET(PORTC, PHASEA, D2A, PHASEB);
      BITS_SET(PORTB, D2B);
      BITS_CLEAR(PORTC, D1A, D0A, D0B);
      BITS_CLEAR(PORTB, D1B);
      break;
    case 1:
      BITS_SET(PORTC, D2A, PHASEB);
      BITS_SET(PORTB, D2B);
      BITS_CLEAR(PORTC, PHASEA, D1A, D0A, D0B);
      BITS_CLEAR(PORTB, D1B);
      break;
    case 2:
      BITS_SET(PORTC, D2A);
      BITS_SET(PORTB, D2B);
      BITS_CLEAR(PORTC, PHASEA, D1A, D0A, PHASEB, D0B);
      BITS_CLEAR(PORTB, D1B);     
      break;
    case 3:
      BITS_SET(PORTC, PHASEA, D2A);
      BITS_SET(PORTB, D2B);
      BITS_CLEAR(PORTC, D1A, D0A, PHASEB, D0B);
      BITS_CLEAR(PORTB, D1B);
      break;
    //default: S_OFF; break;
  }


}


int main(void){

  BITS_SET(DDRB, PB0,PB1,PB2);
  BITS_SET(DDRC, PC5,PC4,PC3,PC2,PC1,PC0);

  BITS_CLEAR(PORTB, PB0);

  _delay_ms(1000);
  int8_t i = 3;
  long int t = 0; 
  int k=1;
  int a = 12000;
  while(1){
    BITS_SET(PORTB, PB0); 
//  _delay_ms(1);
    BITS_CLEAR(PORTB, PB0);
  _delay_us(1000);
  
   t++;
   if (t >= a){
      k^=1;
      t=0;
      a = 3000;

   }
   if (k == 1){
      move(i--);
      if (i<0) i=3;
   } else{
      move(i++);
      if (i>3) i=0;
   }
   

  }
  return 0;
}

